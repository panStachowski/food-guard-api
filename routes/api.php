<?php

use App\Entities\User;
use App\Entities\UserProduct;
use App\Entities\Role;


Route::group([
    //'middleware' => 'auth.api'
], function () {

    Route::resources([
        'products' => 'ProductsController',
        'user_products' => 'UserProductsController',
        'roles' => 'RolesController',
        'users' => 'UsersController',
    ]);

//wyświetli wysztkie produkty usera o id nadanym w find()
   /*  Route::get('/xxx/{id}', function ($id) {

         $test = UserProduct::with('product', 'user')->where('user_id', $id)->get();
         return $test;
     });*/


    Route::post('logout', 'AuthController@logout');
    Route::post('refresh', 'AuthController@refresh');
    Route::get('/user/{id}/user_products', 'UserProductsController@getUserProducts');
});

Route::post('login', 'AuthController@login');
Route::post('register', 'UsersController@store');



