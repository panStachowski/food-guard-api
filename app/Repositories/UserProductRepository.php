<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface UserProductRepository.
 *
 * @package namespace App\Repositories;
 */
interface UserProductRepository extends RepositoryInterface
{
    //
}
