<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;
use App\Http\Requests\UserCreateRequest;
use App\Http\Requests\UserUpdateRequest;
use App\Repositories\UserRepository;
use App\Validators\UserValidator;

/**
 * Class UsersController.
 *
 * @package namespace App\Http\Controllers;
 */
class UsersController extends Controller
{
    /**
     * @var UserRepository
     */
    protected $repository;

    /**
     * @var UserValidator
     */
    protected $validator;

    /**
     * UsersController constructor.
     *
     * @param UserRepository $repository
     * @param UserValidator $validator
     */
    public function __construct(UserRepository $repository, UserValidator $validator)
    {
        $this->repository = $repository;
        $this->validator = $validator;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->repository->pushCriteria(app('Prettus\Repository\Criteria\RequestCriteria'));
        $users = $this->repository->all();


            return response()->json([
                'data' => $users,
            ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param UserCreateRequest $request
     *
     * @return \Illuminate\Http\Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function store(UserCreateRequest $request)
    {
        try {

            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_CREATE);

            $requestData = $request->all();
            $requestData['password'] = bcrypt($requestData['password']);

            $xxx = $request['email'];
            $fat = $request['weight']*1.3;
            $protein = $request['weight']*1.5;
            $carbs = $request['weight']*4;

            $response = [
                'email' => $requestData['email'],
                'password' => $requestData['password'],
                'weight' => $requestData['weight'],
                'height' => $requestData['height'],
                'kcal' => $requestData['kcal'],
                'age' => $requestData['age'],
                'role' => $requestData['role'],
                'sex' => $requestData['sex'],
                'fat' => $fat,
                'protein' => $protein,
                'carbs' => $carbs

            ];

            $user = $this->repository->create($response);

            $response = [
                'message' => 'User created.',
                'data' => $user->toArray(),
            ];

            return response()->json($response);


        } catch (ValidatorException $e) {
            return response()->json([
                'error' => true,
                'message' => $e->getMessageBag()
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = $this->repository->find($id);

        return response()->json([
            'data' => $user,
        ]);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = $this->repository->find($id);

        return view('users.edit', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UserUpdateRequest $request
     * @param string $id
     *
     * @return Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function update(UserUpdateRequest $request, $id)
    {
        try {

            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_UPDATE);
            $requestData = $request->all();


            $fat = $request['weight']*1.3;
            $protein = $request['weight']*1.5;
            $carbs = $request['weight']*4;
            $kcal = (66.5 + ($request['weight'] * 13.7) + (5*$request['height']) - (6.8 * $request['age'])) * 1.1;


            $response = [
                'weight' => $requestData['weight'],
                'height' => $requestData['height'],
                'kcal' => $kcal,
                'age' => $requestData['age'],
                'fat' => $fat,
                'protein' => $protein,
                'carbs' => $carbs

            ];

            $user = $this->repository->update($response, $id);

            $response = [
                'message' => 'User updated.',
                'data' => $user->toArray(),
            ];


            return response()->json($response);


        } catch (ValidatorException $e) {


            return response()->json([
                'error' => true,
                'message' => $e->getMessageBag()
            ]);


        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleted = $this->repository->delete($id);


        return response()->json([
            'message' => 'User deleted.',
            'deleted' => $deleted,
        ]);


    }
}
