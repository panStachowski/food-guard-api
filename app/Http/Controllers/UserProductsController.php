<?php

namespace App\Http\Controllers;

use App\Entities\UserProduct;
use Illuminate\Http\Request;

use App\Http\Requests;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;
use App\Http\Requests\UserProductCreateRequest;
use App\Http\Requests\UserProductUpdateRequest;
use App\Repositories\UserProductRepository;
use App\Validators\UserProductValidator;

/**
 * Class UserProductsController.
 *
 * @package namespace App\Http\Controllers;
 */
class UserProductsController extends Controller
{
    /**
     * @var UserProductRepository
     */
    protected $repository;

    /**
     * @var UserProductValidator
     */
    protected $validator;

    /**
     * UserProductsController constructor.
     *
     * @param UserProductRepository $repository
     * @param UserProductValidator $validator
     */
    public function __construct(UserProductRepository $repository, UserProductValidator $validator)
    {
        $this->repository = $repository;
        $this->validator = $validator;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->repository->pushCriteria(app('Prettus\Repository\Criteria\RequestCriteria'));
        $userProducts = $this->repository->all();
        $requestData = $request['day'];


        return response()->json([
            'data' => $userProducts->where('ate', '==', $requestData)
        ]);

    }

    public function getUserProducts(Request $request, $id)
    {

        $requestData = $request['day'];
        $userProducts = UserProduct::with('product', 'user')->where('user_id', $id)->get();
        $userProducts = $userProducts->where('ate', '==', $requestData);
        $array = [];
        $totalKcal = 0;
        $totalFat = 0;
        $totalProteins = 0;
        $totalCarbs = 0;

        foreach ($userProducts as $item) {
            array_push($array, $item);
            $totalKcal+=$item['kcal'];
            $totalFat+=$item['fat'];
            $totalCarbs+=$item['carbs'];
            $totalProteins+=$item['protein'];
        }

        $response = [
            'data' => $array,
            'totalKcal' => $totalKcal,
            'totalFat' => $totalFat,
            'totalCarbs' => $totalCarbs,
            'totalProtein' => $totalProteins,
        ];

        array_push($response, 'totalKcal');


        return response()->json($response);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  UserProductCreateRequest $request
     *
     * @return \Illuminate\Http\Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function store(UserProductCreateRequest $request)
    {
        try {

            //$this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_CREATE);
            
            $userProduct = $this->repository->create($request->all());

            $response = [
                'message' => 'UserProduct created.',
                'data' => $userProduct->toArray(),
            ];


            return response()->json($response);


        } catch (ValidatorException $e) {
            return response()->json([
                'error' => true,
                'message' => $e->getMessageBag()
            ]);


        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $userProduct = $this->repository->find($id);


        return response()->json([
            'data' => $userProduct,
        ]);


    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $userProduct = $this->repository->find($id);

        return view('userProducts.edit', compact('userProduct'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UserProductUpdateRequest $request
     * @param  string $id
     *
     * @return Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function update(UserProductUpdateRequest $request, $id)
    {
        try {

            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_UPDATE);
            $test = $request['fat'];
            $userProduct = $this->repository->update($request->all(), $id);



            $response = [
                'message' => 'UserProduct updated.',
                'data' => $userProduct->toArray(),
            ];


            //return response()->json($response);
            return $request;


        } catch (ValidatorException $e) {


            return response()->json([
                'error' => true,
                'message' => $e->getMessageBag()
            ]);


        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleted = $this->repository->delete($id);


        return response()->json([
            'message' => 'UserProduct deleted.',
            'deleted' => $deleted,
        ]);


    }
}
