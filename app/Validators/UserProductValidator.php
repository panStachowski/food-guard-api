<?php

namespace App\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

/**
 * Class UserProductValidator.
 *
 * @package namespace App\Validators;
 */
class UserProductValidator extends LaravelValidator
{
    /**
     * Validation Rules
     *
     * @var array
     */
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'weight' => 'required',
            'kcal' => 'required',
            'user_id' => 'required|exists:users,id',
            'product_id' => 'required|exists:products,id'
        ],
        ValidatorInterface::RULE_UPDATE => [],
    ];
}
