<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class UserProduct.
 *
 * @package namespace App\Entities;
 */
class UserProduct extends Model implements Transformable
{
    use TransformableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['product_id', 'user_id', 'weight', 'kcal', 'ate', 'fat', 'protein', 'carbs'];

    public function user() :BelongsTo
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function product() :BelongsTo
    {
        return $this->belongsTo(Product::class);
    }

}
