<?php

use Illuminate\Database\Seeder;

class xxx extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(\App\Repositories\ProductRepository $repository)
    {
        $repository->create([
            'name' => 'Pierś z kurczaka',
            'weight' => '100',
            'kcal' => '98',
            'fat' => '1.3',
            'carbs' => '0',
            'protein' => '21.5',
        ]);
    }
}
