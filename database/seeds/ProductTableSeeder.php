<?php

use Illuminate\Database\Seeder;

class ProductTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(\App\Repositories\ProductRepository $repository)
    {
        $users = [
            [
                'name' => 'Pierś z kurczaka',
                'weight' => '100',
                'kcal' => '98',
                'fat' => '1.3',
                'carbs' => '0',
                'protein' => '21.5',
            ],
            [
                'name' => 'Serek wiejski - OSM Koło',
                'weight' => '100',
                'kcal' => '112',
                'fat' => '6',
                'carbs' => '2.5',
                'protein' => '12',
            ],
            [
                'name' => 'Mleko 1,5% tł. UHT',
                'weight' => '100',
                'kcal' => '44',
                'fat' => '1.5',
                'carbs' => '4.7',
                'protein' => '3',
            ],
            [
                'name' => 'Makaron razowy',
                'weight' => '100',
                'kcal' => '348',
                'fat' => '2.3',
                'carbs' => '70.6',
                'protein' => '11.3',
            ],
            [
                'name' => 'Marchew',
                'weight' => '100',
                'kcal' => '33',
                'fat' => '0.2',
                'carbs' => '5.1',
                'protein' => '1',
            ],
        ];

        foreach($users as $user){
            $repository->create($user);
        }
    }
}
