<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(\App\Repositories\UserRepository $repository)
    {

        $repository->create([
            'email' => 'admin@admin.pl',
            'role' => 'Admin',
            'weight' => '88',
            'height' => '186',
            'kcal' => '2100',
            'fat' => '120',
            'carbs' => '320',
            'protein' => '200',
            'age' => 23,
            'sex' => 'Mężczyzna',
            'password' => bcrypt('qwe')
        ]);
    }
}
